import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { from } from 'rxjs';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService]
})
export class AppComponent implements OnInit{
  input;
  constructor(private userService: UserService){

  }
  ngOnInit(){
    this.input= {
      username: '',
      password: '',
      email:''
    };
  }
  onRegister(){
    this.userService.registerUser(this.input).subscribe(
      response => {
        alert('User ' + this.input.username +' created. ');
      },
      error => {
        console.log('error', error);
      }
      
    );
  }
  
  
}
